// A
console.log("Soal A - Descending Ten")
function DescendingTen(a){
    var hasil = ""
    if (a == null) return -1
    else {
        var temp = a
        for(i = a;i>a-10;i--){
            hasil += temp-- + " "
        }
    }
    return hasil
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

// B
console.log("\nSoal B - Ascending Ten")
function AscendingTen(a){
    var hasil = ""
    if (a == null) return -1
    else {
        var temp = a
        for(i = a;i<a+10;i++){
            hasil += temp++ + " "
        }
    }
    return hasil
}
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

// C
console.log("\nSoal C - Conditional Ascending Descending")
function ConditionalAscDesc(reference,check){
    var hasil = ""
    if(reference==null||check==null) return -1
    else if(check%2==0) hasil = DescendingTen(reference)
    else hasil = AscendingTen(reference)
    return hasil
}
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

// D
console.log("\nSoal D - Papan Ular Tangga")
function ularTangga(){
    var a = 100
    var hasil = ""
    for(i = 0;i<10;i++){
        if(i==0){
            for(j = 0;j<10;j++){
                hasil += a-- + " "
            }
            a++
        }else if(i%2==0){
            a -= 10
            for(j = 0;j<10;j++){
                hasil += a-- + " "
            }
            a++
        }else if(i%2!=0&&i!=0){
            a -= 10
            for(j = 0;j<10;j++){
                hasil += a++ + " "
            }
            a--
        }            
        hasil += "\n"
    }
    return hasil
}
console.log(ularTangga())
