// A
console.log("Soal A - Bandingkan Angka")
function bandingkan(a=0,b=0){
    a = parseInt(a)
    b = parseInt(b)
    if(a<0||b<0) return -1
    else if(a==b) return -1
    else if(a>b) return a
    else return b
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18   

// B
console.log("\nSoal B - Balik String")
function balikString(a){
    var newa = ""
    for(i=a.length-1;i>=0;i--){
        newa += a[i]
    }
    return newa
}
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// C
console.log("\nSoal C - Palindrome")
function palindrome(a){
    var newa = a
    var n = a.length - 1
    for(i=0;i<a.length-1;i++){
        if(a[i]!=a[n]) return false
        n--
    }
    return true
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false