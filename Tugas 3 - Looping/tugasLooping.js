// Soal 1
console.log("Soal 1\n")
console.log("LOOPING PERTAMA")
var a = 1
var b = 21
while(a++ < 20){
    console.log(a + " - I love coding")
}
console.log("LOOPING KEDUA")
while(--b>1){
    console.log(b + " - I will become a mobile developer")
}
console.log("\n")

//Soal 2
console.log("Soal 2")
console.log("OUTPUT")
for(i = 1; i <= 20; i ++){
    if(i%2==0)console.log(i + " - Berkualitas")
    else if(i%3==0)console.log(i + " - I Love Coding")
    else console.log(i + " -  Santai")
}
console.log("\n")

//Soal 3
console.log("Soal 3")
for(i = 0;i<4;i++){
    for(j = 0;j<8;j++){
        process.stdout.write("#")
    }
    process.stdout.write("\n")
}
console.log("\n")

//Soal 4
console.log("Soal 4")
for(i = 0;i<=7;i++){
    for(j = 0;j<i;j++){
        process.stdout.write("#")
    }
    process.stdout.write("\n")
}
console.log("\n")

//Soal 5
console.log("Soal 5")
for(i = 0;i<=7;i++){
    for(j = 0;j<=7;j++){
        if((i+j)%2==0)process.stdout.write(" ")
        else process.stdout.write("#")
    }
    process.stdout.write("\n")
}
console.log("\n")


