//Soal 1
console.log("Soal 1")


function range(a = 0,b = 0){
    var arr = []
    if(a==0||b==0) return -1
    else if (a > b){
        var temp = a
        for(i = 0;i<=a-b;i++){
            arr.push(temp--)
        }
        return arr.sort(function (value1, value2) { return value1 < value2 } ) 
    }
    else{
        var temp = a
        for(i = 0;i<=b-a;i++){
            arr.push(temp++)
        }
        return arr
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal 2
console.log("\nSoal 2")

function rangeWithStep(a = 0,b = 0,c){
    var arr = []
    if(a==0||b==0) return -1
    else if (a > b){
        var temp = a
        for(i = 0;i<=a-b;i++){
            if(temp>=b)arr.push(temp)
            temp-=c        
        }
        return arr.sort(function (value1, value2) { return value1 < value2 } ) 
    }
    else{
        var temp = a
        for(i = 0;i<=b-a;i++){
            if(temp<=b)arr.push(temp)
            temp+=c
        }
        return arr
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4))

//Soal 3
console.log("\nSoal 3")
function sum(a = 0,b = 0,c=1){
    var sum = 0
    if (a > b){
        var temp = a
        for(i = 0;i<=a-b;i++){
            if(temp>=b)sum+=temp  
            temp-=c
        }
        return sum
    }
    else{
        var temp = a
        for(i = 0;i<=b-a;i++){
            if(temp<=b)sum+=temp
            temp+=c
        }
        return sum
    }
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//Soal 4
console.log("\nSoal 4")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
function dataHandling(a){
    for(i = 0 ;i<a.length;i++){
        process.stdout.write("Nomor ID: " + a[i][0] + "\nNama Lengkap: " + a[i][1]+"\nTTL: " 
        + a[i][2]+" " + a[i][3] + "\nHobi: " + a[i][4])
        process.stdout.write("\n\n")
    }
}
dataHandling(input)

//Soal 5
console.log("\nSoal 5")
function balikKata(a){
    var newa = ""
    for(i=a.length-1;i>=0;i--){
        newa += a[i]
    }
    return newa
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar"))// racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal 6
console.log("\nSoal 6")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function urut(a){
    for(i = 0;i<a.length;i++){
        a[i]=parseInt(a[i])
    }
    a.sort(function(a, b){return b-a});
    for(i = 0;i<a.length;i++){
        a[i]=(''+a[i])
    }
    return a
}
function dataHandling2(a){
    a.splice(1,2,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung")
    a.splice(4,1)
    a.splice(4,0,"Pria","SMA Internasional Metro")
    var date = a[3].split("/")
    console.log(a)
    var bulan = date[1]
    switch(bulan){
        case '01':
            console.log("Januari")
            break;
        case '02':
            console.log("Februari")
            break;    
        case '03':
            console.log("Maret")
            break;        
        case '04':
            console.log("April")
            break;       
        case '05':
            console.log("Mei")
            break;
        case '06':
            console.log("Juni")
            break;
        case '07':
            console.log("Juli")
            break;
        case '08':
            console.log("Agustus")
            break;    
        case '09':
            console.log("September")
            break;        
        case '10':
            console.log("Oktober")
            break;       
        case '11':
            console.log("November")
            break;
        case '12':
            console.log("Desember")
            break;
    }
    
    var newdata = date.join("-")
    console.log(date.sort((a,b)=>{
        return b-a;
    }))
    console.log(newdata)
    var b = a[1].slice(0,15)
    console.log(b)
}
dataHandling2(input)
