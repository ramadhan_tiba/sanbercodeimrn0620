//Soal 1
console.log("Soal 1 - Class Score")
let points = []
class Score{
    
    constructor(subject,points,email){
        this.subject = subject
        this.points = points
        this.email = email
    }
    average = () =>{
        let jumlah = 0
        let hasil = 0
        let i = 0
        for(i = 0;i<this.points.length;i++){
            jumlah += this.points[i]
        }
            hasil = jumlah/this.points.length
            console.log(hasil)            
        return () => {}
    }
}
// jika inputan berupa 1 nilai
let test = new Score("a",10,"b")
console.log(test.points)
// jika inputan berupa array of number
let test2 = new Score("a",[20,50,90],"b")
test2.average()

//Soal 2
console.log("\nSoal 2 - Create Score")
const viewScores = (data,subject) =>{
    let ouput = []
    for(i = 1;i<data.length;i++){
        let hasil = {
            email: null,
            subject: null,
            points: null
        }
        if(subject == "quiz-1"){
            hasil.email = data[i][0]
            hasil.subject = subject
            hasil.points = data[i][1]
        }else if (subject == "quiz-2"){
            hasil.email = data[i][0]
            hasil.subject = subject
            hasil.points = data[i][2]
        }else if (subject == "quiz-3"){
            hasil.email = data[i][0]
            hasil.subject = subject
            hasil.points = data[i][3]
        }
        ouput.push(hasil)
    } 
    console.log(ouput)
    return () =>{}
}

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

//Soal 3
console.log("\nSoal 3 - Recap Score")

const recapScores = (data) =>{
    for(i = 1;i<data.length;i++){
        let hasil = {
            email: null,
            points: [],
            predikat: null
        }
        hasil.email = data[i][0]
        let jumlah = 0
        for(j = 1;j<data[i].length;j++) jumlah += data[i][j]
        hasil.points = jumlah/(data[i].length-1)
        if(hasil.points>90)hasil.predikat = "honour"
        else if(hasil.points>80)hasil.predikat = "graduate"
        else hasil.predikat = "participant"
        let string = `${i}. Email: ${hasil.email}\nRata-rata: ${hasil.points}\nPredikat: ${hasil.predikat}\n`
        console.log(string)
        
    } 

    return () =>{}
}
recapScores(data)
