// Soal 1 
console.log("Soal 1")
function arrayToObject(a){
    if(a!=null){
        var hasil ={
            firstName: null,
            lastName: null,
            gender: null,
            age: 0,
        }
        var tanggal = new Date()
        var tangnow = tanggal.getFullYear()
        for(i = 0;i<a.length;i++){
            hasil.firstName = a[i][0]
            hasil.lastName = a[i][1]
            hasil.gender = a[i][2]
            if(a[i][3]==null||tangnow - a[i][3]<0)  hasil.age = "Invalid birth year"
            else if(tangnow - a[i][3]>0){
                hasil.age = tangnow - a[i][3]
            }
            process.stdout.write(i+1 + ". " + hasil.firstName +" " +hasil.lastName + " : ")
            console.log(hasil)
        }
    }
    else console.log("")
    
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

// Soal 2
console.log("\nSoal 2")
var item = [["Sepatu Stacattu",1500000],["Baju Zoro",500000],["Baju H&N",250000],["Sweater Uniklooh",175000],["Casing Handphone",50000]]
function shoppingTime(memberId, money){
    var data = {
        memberId:"",
        money : 0,
        listPurchased : [],
        changeMoney : 0
    }
    var a = 0
    if(memberId==""||memberId==null) return "Mohon maaf, toko X hanya berlaku untuk member saja"
    else if(money < 50000) return "Mohon maaf, uang tidak cukup"
    else {
        data.memberId = memberId
        data.money = money
        for(i = 0;i<item.length;i++){
            if(money>0){
                if(money-item[i][1]>=0){
                    data.listPurchased[a++] = item[i][0]
                    money -= item[i][1]
                }
            }
        }
        data.changeMoney = money
    }
    return data
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime());


// Soal 3
console.log("\nSoal 3")
function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var hasil = []
    for(i = 0;i<listPenumpang.length;i++){
        var data = {
            penumpang : "",
            naikDari : "",
            tujuan : "",
            bayar : 0
        }
        var harga = (rute.indexOf(listPenumpang[i][2])-rute.indexOf(listPenumpang[i][1]))*2000
        data.penumpang = listPenumpang[i][0]
        data.naikDari = listPenumpang[i][1]
        data.tujuan = listPenumpang[i][2]
        data.bayar = harga
        hasil[i] = data
    }
    return hasil
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]

