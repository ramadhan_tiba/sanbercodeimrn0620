//Soal 1
console.log("Soal 1")
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word.concat(" ",second," ",third," ",fourth," ",fifth," ",sixth," ",seventh)+"\n");

//Soal 2
console.log("Soal 2")
function cutting(str, start, end) {
    var newStr = "";
    for(i = start; i <= end ;i++){
        newStr += str[i]
    }
    return newStr
  }

var sentence = "I am going to be React Native Developer"; 
var firstWord = sentence[0] ; //I
var secondWord = sentence[2] + sentence[3]  ; //am
var thirdWord = cutting(sentence,5,9);// going 
var fourthWord = sentence[11] + sentence[12]; // to
var fifthWord = sentence[14] + sentence[15]; // be
var sixthWord = cutting(sentence,17,22); // React
var seventhWord = cutting(sentence,23,29);//Native
var lastWord = cutting(sentence,30,38);// Developer
console.log('First Word: ' + firstWord) 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord);
console.log('Last Word: ' + lastWord + "\n");

//Soal 3
console.log("Soal 3")
var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); //wow
var secondWord2 = sentence2.substring(4,14); // JavaScript
var thirdWord2 = sentence2.substring(15,17); // is
var fourthWord2 = sentence2.substring(18,20); // so
var fifthWord2 = sentence2.substring(21,25); // cool
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2 + "\n");   

//Soal 4
console.log("Soal 4")
var sentence3 = 'wow JavaScript is so cool'; 
var firstWord3 = sentence3.substring(0, 3); //wow
var secondWord3 = sentence3.substring(4,14); // JavaScript
var thirdWord3 = sentence3.substring(15,17); // is
var fourthWord3 = sentence3.substring(18,20); // so
var fifthWord3 = sentence3.substring(21,25); // cool
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + firstWord3 + ', with length: ' + firstWord3.length); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3.length ); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3.length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3.length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3.length + "\n"); 

//Soal 5
console.log("Soal 5")
var nama = "John"
var peran = "Warewolf"

if(nama=="" && peran=="")console.log("Nama harus diisi!")
else if(nama!="" && peran=="")console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
else if(nama!="" && peran=="Penyihir")console.log("Selamat datang di Dunia Werewolf, " + nama + 
"\nHalo" +peran + " " + nama +", kamu dapat melihat siapa yang menjadi werewolf!")
else if(nama!="" && peran=="Guard")console.log("Selamat datang di Dunia Werewolf, " + nama + 
"\nHalo" +peran + " " + nama +",  kamu akan membantu melindungi temanmu dari serangan werewolf")
else if(nama!="" && peran=="Warewolf")console.log("Selamat datang di Dunia Werewolf, " + nama + 
"\nHalo" +peran + " " + nama +", Kamu akan memakan mangsa setiap malam!")

console.log("\n")

//Soal 6
console.log("Soal 6")
var hari = 21; 
var bulan = 5; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
switch(bulan){
    case 1:
        console.log(hari + " Januari " + tahun + "\n")
        break;
    case 2:
        console.log(hari + " Februari " + tahun + "\n")
        break;    
    case 3:
        console.log(hari + " Maret " + tahun + "\n")
        break;        
    case 4:
        console.log(hari + " April " + tahun + "\n")
        break;       
    case 5:
        console.log(hari + " Mei " + tahun + "\n")
        break;
    case 6:
        console.log(hari + " Juni " + tahun + "\n")
        break;
    case 7:
        console.log(hari + " Juli " + tahun + "\n")
        break;
    case 8:
        console.log(hari + " Agustus " + tahun + "\n")
        break;    
    case 9:
        console.log(hari + " September " + tahun + "\n")
        break;        
    case 10:
        console.log(hari + " Oktober " + tahun + "\n")
        break;       
    case 11:
        console.log(hari + " November " + tahun + "\n")
        break;
    case 12:
        console.log(hari + " Desember " + tahun + "\n")
        break;
}