//Soal 1
console.log("Soal 1")
const golden = () => {
    console.log("this is golden!!")
    return () =>{}
}
golden()

//Soal 2
console.log("\nSoal 2")
const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  //Driver Code 
  newFunction("William", "Imoh").function() 

//Soal 3
console.log("\nSoal 3")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const { firstName, lastName ,destination , occupation } = newObject;
console.log(firstName, lastName, destination, occupation)


//Soal 4
console.log("\nSoal 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [west,east]
//Driver Code
console.log(combinedArray)

//Soal 5
console.log("\nSoal 5")
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
const after  =`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim  ad minim veniam`
 
// Driver Code
console.log(after)
