import React, { Component } from 'react';
import {View,Text,StyleSheet,Platform,Image,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class SkillItem extends Component{
    render(){
        let skill = this.props.skill

        let imagelogo= [
            require('./images/react.png'),
            require('./images/laravel.png'),
            require('./images/javascript.png'),
            require('./images/python.png'),
            require('./images/git.png'),
            require('./images/gitlab.png'),
        ]
        
        
        return(
            <View style={styles.container}>
                <Image source={imagelogo[skill.id-1]} style={styles.logo}/>                    
                <View style={styles.container2}> 
                    <Text style={styles.skillTitle}>{skill.skillName}</Text>
                    <Text style={styles.skillTitle}>{skill.categoryName}</Text>                
                </View>
                <Text style={styles.skillTitle}>{skill.percentageProgress}</Text>
            </View>
        )
    }
 }


const styles = StyleSheet.create({
    container: {
        backgroundColor:'#9F2523',
        padding: 15,
        flexDirection:'row'
    },container2: {
        flex:1,
        marginStart:10
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 15
    },
    skillTitle: {
        fontSize: 16,
        color: '#ffffff'
    },
    videoDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    videoStats: {
        fontSize: 15,
        paddingTop: 3,
        color:"#888888"
    },logo:{
        width:41,
        height:45
    }
})