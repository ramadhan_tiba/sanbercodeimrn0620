import React,{Component} from 'react'
import {Text,View,StyleSheet,Image,TextInput,FlatList} from 'react-native'
import Data from './skillData.json'
import SkillItem from './skillItems'



export default class AboutScreen extends Component{
    
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.text}>My Skill</Text>
                <View style={styles.box2}>
                    <Text style={styles.text2}>ramadhan_tiba</Text>
                </View>
                <View style={styles.body}>
                    <FlatList
                        data={Data.items}
                        renderItem={(skill)=><SkillItem skill={skill.item} />}
                        keyExtractor={(item)=>item.id}
                        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}

                    />  
                </View>                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:20,
        backgroundColor:'#F499A2'
    },container2:{
        flexDirection:"row",
        padding:5,
        backgroundColor:'#9F2523'
    },text:{
        fontSize:30,
        color:'#000000',
        textAlign:"center",
        marginBottom:20
    },text2:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
    },text3:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
        marginBottom:20,
        marginTop:10
    },textinput:{
        height: 50,
        fontSize:20,
        backgroundColor:'#ffffff',
        marginStart:20,
        marginEnd:20,
        padding:5,
        borderRadius:5
    },ochako:{
        height:166,
        width:144,
        marginStart:95,
    },box:{
        padding:5,
        backgroundColor:'#9F2523',
        borderRadius:5,
        height:269,
        width:272,
        marginStart:30,
        marginTop:30
    },logo:{
        width:41,
        height:45
    },textSkill:{
        fontSize:19,
        color:'#ffffff',
        margin:10
    },box2:{
        padding:10,
        backgroundColor:'#9F2523',
        borderRadius:5,
        marginStart:70,
        marginEnd:70
    },body:{
        flex:1,
        backgroundColor:'#9F2523',
        borderRadius:5,
        marginTop:20
    }
})