import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { Login, About, Skill,Add, Project } from './Screen'

const LoginStack = createStackNavigator()
const HomeStack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator();


const TabsScreen = () =>(
    <Tabs.Navigator>
      <Tabs.Screen name="Skill" component={Skill} />
      <Tabs.Screen name="Add" component={Add} />
      <Tabs.Screen name="Project" component={Project} />
    </Tabs.Navigator>
  )


const Beranda = () =>(
        <Drawer.Navigator>
            <Drawer.Screen name='Home' component={TabsScreen}/>
            <Drawer.Screen name='About Me' component={About}/>
      </Drawer.Navigator>
)


export default () =>(
    <NavigationContainer>
        <LoginStack.Navigator>
            <LoginStack.Screen name="Login" component={Login}/>
            <LoginStack.Screen name="Beranda" component={Beranda}/>
        </LoginStack.Navigator>
    </NavigationContainer>
)