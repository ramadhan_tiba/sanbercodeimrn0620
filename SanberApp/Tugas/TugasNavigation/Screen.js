import React from "react";
import { View, Text, StyleSheet,Image,TextInput ,ScrollView,FlatList} from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler";
import Data from './skillData.json'
import SkillItem from './skillItems'



const styles = StyleSheet.create({
    container: {
      flex: 1,
      
    },
    button: {
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginVertical: 10,
      borderRadius: 5
    }
  });
  
  const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );
  

export const Login = ({navigation}) =>{
    return(
        <ScreenContainer>
        <View style={stylesLogin.container}>
               <Image source={require('./images/ochako.png')}  style={stylesLogin.ochako}></Image> 
               <Text style={stylesLogin.text}>LOGIN</Text>  
               <View >
                   <Text style={stylesLogin.text2}>Username</Text>
                   <TextInput
                    style = {stylesLogin.textinput}
                   />
               </View>            
               <View >
                   <Text style={stylesLogin.text2}>Password</Text>
                   <TextInput
                    style = {stylesLogin.textinput}
                    secureTextEntry={true}
                    />
               </View> 
               <TouchableOpacity onPress={() => navigation.push("Beranda")}>
                   <View style={stylesLogin.box} >
                   <Text style={stylesLogin.text3}>Sign</Text>
               </View>
               </TouchableOpacity>
               
           </View>
   </ScreenContainer>
    )
}

export const About = () => {
    return(
        <ScreenContainer>
            <ScrollView>
             <View style={stylesAbout.container}>
                <Text style={stylesAbout.text}>About Me</Text>
                <Image source={require('./images/ojan.png')}  style={stylesAbout.ochako}></Image> 
                <Text style={stylesAbout.text2}>Muhammad Fauzan Ramadhan</Text>  
                <View style={stylesAbout.box}>
                    <View style={stylesAbout.container2}>
                        <Image source={require('./images/twitter.png')} style={stylesAbout.logo}></Image>
                        <Text style={stylesAbout.textSkill}>ramadhanazuaf</Text>
                    </View>
                    <View style={stylesAbout.container2}>
                        <Image source={require('./images/instagram.png')} style={stylesAbout.logo}></Image>
                        <Text style={stylesAbout.textSkill}>ramadhanazuaf</Text>
                    </View>
                    <View style={stylesAbout.container2}>
                        <Image source={require('./images/facebook.png')} style={stylesAbout.logo}></Image>
                        <Text style={stylesAbout.textSkill}>Fauzan Ramadhan</Text>
                    </View>
                    <View style={stylesAbout.container2}>
                        <Image source={require('./images/gitlab.png')} style={stylesAbout.logo}></Image>
                        <Text style={stylesAbout.textSkill}>https://gitlab.com/ramadhan_tiba</Text>
                    </View>
                </View>
            </View>
            </ScrollView>
        </ScreenContainer>
    )
}

export const Skill = () => {
    return(
        <ScreenContainer>
            <ScrollView>
            <View style={stylesSkill.container}>
                <Text style={stylesSkill.text}>My Skill</Text>
                <View style={stylesSkill.box2}>
                    <Text style={stylesSkill.text2}>ramadhan_tiba</Text>
                </View>
                <View style={stylesSkill.body}>
                    <FlatList
                        data={Data.items}
                        renderItem={(skill)=><SkillItem skill={skill.item} />}
                        keyExtractor={(item)=>item.id}
                        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}

                    />  
                </View>                
            </View>
            </ScrollView>
        </ScreenContainer>
    )

}

export const Add = () => (
    <ScreenContainer>
    <Text>Halaman Tambah</Text>
  </ScreenContainer>
)
export const Project = () => (
    <ScreenContainer>
    <Text>Halaman Projek</Text>
  </ScreenContainer> 
)
   

const stylesLogin = StyleSheet.create({
    container:{
        flex:1,
        padding:15,
        backgroundColor:'#F499A2'
        
    },text:{
        fontSize:30,
        color:'#000000',
        textAlign:"center",
        marginBottom:20
    },text2:{
        color:'#000000',
        marginStart:20,
        marginBottom:5,
        marginTop:5
    },text3:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
        marginBottom:20,
        marginTop:10
    },textinput:{
        height: 50,
        fontSize:20,
        backgroundColor:'#ffffff',
        marginStart:20,
        marginEnd:20,
        padding:5,
        borderRadius:5
    },ochako:{
        height:157,
        width:259,
        justifyContent: "space-evenly",
        margin:30
    },box:{
        backgroundColor:'#9F2523',
        borderRadius:5,
        height:40,
        width:89,
        marginStart:120,
        marginTop:30
    }
})

const stylesAbout = StyleSheet.create({
    container:{
        flex:1,
        padding:15,
        backgroundColor:'#F499A2'
    },container2:{
        flexDirection:"row",
        padding:5,
        backgroundColor:'#9F2523'
    },text:{
        fontSize:30,
        color:'#000000',
        textAlign:"center",
        marginBottom:20
    },text2:{
        fontSize:25,
        color:'#000000',
        textAlign:"center",
        marginTop:10
    },text3:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
        marginBottom:20,
        marginTop:10
    },textinput:{
        height: 50,
        fontSize:20,
        backgroundColor:'#ffffff',
        marginStart:20,
        marginEnd:20,
        padding:5,
        borderRadius:5
    },ochako:{
        height:166,
        width:144,
        marginStart:95,
    },box:{
        padding:5,
        backgroundColor:'#9F2523',
        borderRadius:5,
        height:269,
        width:272,
        marginStart:30,
        marginTop:30
    },logo:{
        width:41,
        height:45
    },textSkill:{
        fontSize:19,
        color:'#ffffff',
        margin:10
    }
})

const stylesSkill = StyleSheet.create({
    container:{
        flex:1,
        padding:20,
        backgroundColor:'#F499A2'
    },container2:{
        flexDirection:"row",
        padding:5,
        backgroundColor:'#9F2523'
    },text:{
        fontSize:30,
        color:'#000000',
        textAlign:"center",
        marginBottom:20
    },text2:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
    },text3:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
        marginBottom:20,
        marginTop:10
    },textinput:{
        height: 50,
        fontSize:20,
        backgroundColor:'#ffffff',
        marginStart:20,
        marginEnd:20,
        padding:5,
        borderRadius:5
    },ochako:{
        height:166,
        width:144,
        marginStart:95,
    },box:{
        padding:5,
        backgroundColor:'#9F2523',
        borderRadius:5,
        height:269,
        width:272,
        marginStart:30,
        marginTop:30
    },logo:{
        width:41,
        height:45
    },textSkill:{
        fontSize:19,
        color:'#ffffff',
        margin:10
    },box2:{
        padding:10,
        backgroundColor:'#9F2523',
        borderRadius:5,
        marginStart:70,
        marginEnd:70
    },body:{
        flex:1,
        backgroundColor:'#9F2523',
        borderRadius:5,
        marginTop:20
    }
})