import React,{Component} from 'react'
import {Text,View,StyleSheet,Image,TextInput} from 'react-native'

export default class AboutScreen extends Component{
    
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.text}>About Me</Text>
                <Image source={require('./images/ojan.png')}  style={styles.ochako}></Image> 
                <Text style={styles.text2}>Muhammad Fauzan Ramadhan</Text>  
                <View style={styles.box}>
                    <View style={styles.container2}>
                        <Image source={require('./images/twitter.png')} style={styles.logo}></Image>
                        <Text style={styles.textSkill}>ramadhanazuaf</Text>
                    </View>
                    <View style={styles.container2}>
                        <Image source={require('./images/instagram.png')} style={styles.logo}></Image>
                        <Text style={styles.textSkill}>ramadhanazuaf</Text>
                    </View>
                    <View style={styles.container2}>
                        <Image source={require('./images/facebook.png')} style={styles.logo}></Image>
                        <Text style={styles.textSkill}>Fauzan Ramadhan</Text>
                    </View>
                    <View style={styles.container2}>
                        <Image source={require('./images/gitlab.png')} style={styles.logo}></Image>
                        <Text style={styles.textSkill}>https://gitlab.com/ramadhan_tiba</Text>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:15,
        backgroundColor:'#F499A2'
    },container2:{
        flexDirection:"row",
        padding:5,
        backgroundColor:'#9F2523'
    },text:{
        fontSize:30,
        color:'#000000',
        textAlign:"center",
        marginBottom:20
    },text2:{
        fontSize:25,
        color:'#000000',
        textAlign:"center",
        marginTop:10
    },text3:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
        marginBottom:20,
        marginTop:10
    },textinput:{
        height: 50,
        fontSize:20,
        backgroundColor:'#ffffff',
        marginStart:20,
        marginEnd:20,
        padding:5,
        borderRadius:5
    },ochako:{
        height:166,
        width:144,
        marginStart:95,
    },box:{
        padding:5,
        backgroundColor:'#9F2523',
        borderRadius:5,
        height:269,
        width:272,
        marginStart:30,
        marginTop:30
    },logo:{
        width:41,
        height:45
    },textSkill:{
        fontSize:19,
        color:'#ffffff',
        margin:10
    }
})