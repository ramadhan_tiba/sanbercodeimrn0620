import React,{Component} from 'react'
import {Text,View,StyleSheet,Image,TextInput} from 'react-native'

export default class LoginScreen extends Component{
    
    render(){
        return(
            <View style={styles.container}>
                <Image source={require('./images/ochako.png')}  style={styles.ochako}></Image> 
                <Text style={styles.text}>LOGIN</Text>  
                <View >
                    <Text style={styles.text2}>Username</Text>
                    <TextInput
                     style = {styles.textinput}
                    />
                </View>            
                <View >
                    <Text style={styles.text2}>Password</Text>
                    <TextInput
                     style = {styles.textinput}
                     secureTextEntry={true}
                     />
                </View> 
                <View style={styles.box}>
                    <Text style={styles.text3}>Sign</Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:15,
        backgroundColor:'#F499A2'
        
    },text:{
        fontSize:30,
        color:'#000000',
        textAlign:"center",
        marginBottom:20
    },text2:{
        color:'#000000',
        marginStart:20,
        marginBottom:5,
        marginTop:5
    },text3:{
        fontSize:15,
        color:'#ffffff',
        textAlign:"center",
        marginBottom:20,
        marginTop:10
    },textinput:{
        height: 50,
        fontSize:20,
        backgroundColor:'#ffffff',
        marginStart:20,
        marginEnd:20,
        padding:5,
        borderRadius:5
    },ochako:{
        height:157,
        width:259,
        justifyContent: "space-evenly",
        margin:30
    },box:{
        backgroundColor:'#9F2523',
        borderRadius:5,
        height:40,
        width:89,
        marginStart:120,
        marginTop:30
    }
})