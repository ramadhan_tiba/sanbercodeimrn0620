import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      value: 'Useless Placeholder',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style = {styles.text}
          onChangeText={(text) => this.setState({ value: text })}
          value={this.state.value}
        />
        <Text
        style = {styles.output}>
            {this.state.value}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{flex: 1,justifyContent: "center",paddingHorizontal: 10}
    ,text:{ height: 40, borderColor: 'gray', borderWidth: 1,padding:5 }
    ,output:{fontSize:50,margin:20,textAlign:'center'}
});